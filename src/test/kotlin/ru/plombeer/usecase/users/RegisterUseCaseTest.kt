package ru.plombeer.usecase.users

import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import ru.plombeer.repository.UserService
import ru.plombeer.views.request.RegisterRequest

class RegisterUseCaseTest {

    private val userService = mockk<UserService>()

    @Test
    fun `test we don't create another user`() {
        coEvery { userService.findByEmail(any()) } returns mockk()

        val registerUseCase = RegisterUseCase(
            userService = userService,
            hashUseCase = HashUseCase(),
            mapUserEntityUseCase = MapUserEntityUseCase()
        )

        val request = RegisterRequest("Name", "Email", "password")
        assertThrows<IllegalArgumentException> {
            runBlocking { registerUseCase.invoke(request) }
        }

        coVerify(inverse = true) { userService.addUser(any(), any(), any()) }

    }
}