package ru.plombeer.usecase.users

import ru.plombeer.repository.UserService
import ru.plombeer.views.response.UserView

class FindUserByEmailUseCase(
    private val userService: UserService,
    private val mapUserEntityUseCase: MapUserEntityUseCase
){

    suspend fun invoke(email: String): UserView? {
        val userEntity = userService.findByEmail(email)
        return userEntity?.let {
            mapUserEntityUseCase.invoke(userEntity)
        }
    }
}