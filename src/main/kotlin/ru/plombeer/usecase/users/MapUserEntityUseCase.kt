package ru.plombeer.usecase.users

import ru.plombeer.repository.entity.UserEntity
import ru.plombeer.views.response.UserView

class MapUserEntityUseCase {

    fun invoke(userEntity: UserEntity): UserView {
        return UserView(
            id = userEntity.id,
            name = userEntity.name
        )
    }
}