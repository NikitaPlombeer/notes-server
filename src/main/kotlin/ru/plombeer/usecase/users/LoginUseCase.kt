package ru.plombeer.usecase.users

import ru.plombeer.repository.UserService
import ru.plombeer.views.request.UserPasswordCredential
import ru.plombeer.views.response.UserView

class LoginUseCase(
    private val userService: UserService,
    private val hashUseCase: HashUseCase,
    private val mapUserEntityUseCase: MapUserEntityUseCase
){

    suspend fun invoke(credential: UserPasswordCredential): UserView {
        val userEntity = userService.findByEmail(credential.email)
        require(userEntity != null) { "No user found" }

        val requestPasswordHash = hashUseCase.invoke(credential.password)
        if(requestPasswordHash == userEntity.password) {
            return mapUserEntityUseCase.invoke(userEntity)
        }
        throw IllegalAccessException("Email and password don't match")
    }
}