package ru.plombeer.usecase.users

import ru.plombeer.repository.UserService
import ru.plombeer.views.response.UserView

class FindUserByIdUseCase(
    private val userService: UserService,
    private val mapUserEntityUseCase: MapUserEntityUseCase
){

    suspend fun invoke(id: Long): UserView? {
        val userEntity = userService.findById(id)
        return userEntity?.let {
            mapUserEntityUseCase.invoke(userEntity)
        }
    }
}