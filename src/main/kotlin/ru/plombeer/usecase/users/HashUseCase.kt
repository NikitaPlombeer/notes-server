package ru.plombeer.usecase.users

import java.nio.charset.Charset
import java.security.MessageDigest
import javax.xml.bind.DatatypeConverter


class HashUseCase {

    fun invoke(value: String): String {
        val msdDigest = MessageDigest.getInstance("SHA-1")
        val charset = Charset.forName("UTF-8")
        val bytes = value.toByteArray(charset)
        msdDigest.update(bytes, 0, value.length)
        return DatatypeConverter.printHexBinary(msdDigest.digest())
    }
}