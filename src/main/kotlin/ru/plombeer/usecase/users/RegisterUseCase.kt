package ru.plombeer.usecase.users

import ru.plombeer.repository.UserService
import ru.plombeer.views.request.RegisterRequest
import ru.plombeer.views.response.UserView

class RegisterUseCase(
    private val userService: UserService,
    private val hashUseCase: HashUseCase,
    private val mapUserEntityUseCase: MapUserEntityUseCase
){

    suspend fun invoke(request: RegisterRequest): UserView {
        val entityByEmail = userService.findByEmail(request.email)
        require(entityByEmail == null) { "User already exists" }

        val newUserEntity = userService.addUser(
            email = request.email,
            password = hashUseCase.invoke(request.password),
            name = request.name
        )

        return mapUserEntityUseCase.invoke(newUserEntity)
    }
}