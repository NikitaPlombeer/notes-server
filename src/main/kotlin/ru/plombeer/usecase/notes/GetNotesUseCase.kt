package ru.plombeer.usecase.notes

import ru.plombeer.repository.NotesService
import ru.plombeer.repository.entity.Notes
import ru.plombeer.views.response.NoteView

class GetNotesUseCase(
    private val notesService: NotesService
){

    suspend fun invoke(uid: Long): List<NoteView> {
        return notesService.findAll(uid).map {
            NoteView(
                id = it.id,
                title = it.title,
                description = it.description
            )
        }
    }
}