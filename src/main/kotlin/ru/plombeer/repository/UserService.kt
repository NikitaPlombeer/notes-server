package ru.plombeer.repository

import ru.plombeer.repository.entity.UserEntity

interface UserService {

    suspend fun findById(id: Long): UserEntity?
    suspend fun findByEmail(email: String): UserEntity?
    suspend fun addUser(email: String, password: String, name: String): UserEntity
}