package ru.plombeer.repository

import me.liuwj.ktorm.dsl.eq
import me.liuwj.ktorm.dsl.insertAndGenerateKey
import me.liuwj.ktorm.entity.findById
import me.liuwj.ktorm.entity.findOne
import ru.plombeer.repository.entity.UserEntity
import ru.plombeer.repository.entity.Users

class UserRepository : UserService {

    override suspend fun findById(id: Long) = Users.findById(id)

    override suspend fun findByEmail(email: String) = Users.findOne {
        it.email.getColumn() eq email
    }

    override suspend fun addUser(
        email: String,
        password: String,
        name: String
    ): UserEntity {
        val userId = Users.insertAndGenerateKey {
            it.email.getColumn() to email
            it.password.getColumn() to password
            it.name.getColumn() to name
        } as Long

        return findById(userId)!!
    }

}