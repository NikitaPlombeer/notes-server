package ru.plombeer.repository.entity

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.Table
import me.liuwj.ktorm.schema.long
import me.liuwj.ktorm.schema.varchar

object Users : Table<UserEntity>("users") {
    val id = long("id").primaryKey().bindTo { it.id }
    val email = varchar("email").bindTo { it.email }
    val name = varchar("name").bindTo { it.name }
    val password = varchar("password").bindTo { it.password }
}

interface UserEntity : Entity<UserEntity> {
    val id: Long
    val name: String
    val email: String
    val password: String
}
