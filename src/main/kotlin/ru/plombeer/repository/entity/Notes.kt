package ru.plombeer.repository.entity

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.Table
import me.liuwj.ktorm.schema.long
import me.liuwj.ktorm.schema.varchar

object Notes : Table<NotesEntity>("notes") {
    val id = long("id").primaryKey().bindTo { it.id }
    val title = varchar("title").bindTo { it.title }
    val description = varchar("description").bindTo { it.description }
    val userId = long("user_id").bindTo { it.userId }
}

interface NotesEntity : Entity<NotesEntity> {
    val id: Long
    val title: String
    val description: String
    val userId: Long
}