package ru.plombeer.repository

import me.liuwj.ktorm.dsl.eq
import me.liuwj.ktorm.entity.findList
import ru.plombeer.repository.entity.Notes
import ru.plombeer.repository.entity.NotesEntity

class NotesRepository : NotesService {

    override suspend fun findAll(uid: Long): List<NotesEntity> {
        return Notes.findList {
            it.userId.getColumn() eq uid
        }
    }

}