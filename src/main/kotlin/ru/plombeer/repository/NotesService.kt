package ru.plombeer.repository

import ru.plombeer.repository.entity.NotesEntity

interface NotesService {

    suspend fun findAll(uid: Long): List<NotesEntity>

}