package ru.plombeer.views.request

import io.ktor.auth.Credential

data class UserPasswordCredential(
    val email: String,
    val password: String
) : Credential
