package ru.plombeer.views.request

class RegisterRequest(
    val name: String,
    val email: String,
    val password: String
)