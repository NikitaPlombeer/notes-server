package ru.plombeer.views.response

class NoteView(
    val id: Long,
    val title: String,
    val description: String
)