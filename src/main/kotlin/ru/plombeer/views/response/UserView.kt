package ru.plombeer.views.response

import io.ktor.auth.Principal

class UserView(
    val id: Long,
    val name: String
) : Principal