package ru.plombeer.modules

import org.koin.dsl.module
import ru.plombeer.repository.NotesRepository
import ru.plombeer.repository.NotesService
import ru.plombeer.repository.UserService
import ru.plombeer.repository.UserRepository

val repositoryModule = module {
    single<UserService> { UserRepository() }
    single<NotesService> { NotesRepository() }
}