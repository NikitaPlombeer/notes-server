package ru.plombeer.modules

import org.koin.core.KoinComponent
import org.koin.core.qualifier.named
import org.koin.dsl.module
import ru.plombeer.controllers.AuthController
import ru.plombeer.controllers.Controller
import ru.plombeer.controllers.NotesController

val controllerModule = module {
    single<Controller>(named("auth")) { AuthController(get(), get(), get(), get()) }
    single<Controller>(named("notes")) { NotesController(get()) }
}

inline fun <reified T : Controller> KoinComponent.injectControllers(): Lazy<List<T>> = lazy {
    getKoin().getAll<T>().sortedByDescending { it.order }
}