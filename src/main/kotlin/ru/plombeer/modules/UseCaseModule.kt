package ru.plombeer.modules

import org.koin.dsl.module
import ru.plombeer.usecase.JwtService
import ru.plombeer.usecase.notes.GetNotesUseCase
import ru.plombeer.usecase.users.*

val useCaseModule = module {
    //login
    single { JwtService() }
    single { HashUseCase() }
    single { LoginUseCase(get(), get(), get()) }
    single { RegisterUseCase(get(), get(), get()) }

    //users
    single { FindUserByIdUseCase(get(), get()) }
    single { FindUserByEmailUseCase(get(), get()) }
    single { MapUserEntityUseCase() }

    //notes
    single { GetNotesUseCase(get()) }
}