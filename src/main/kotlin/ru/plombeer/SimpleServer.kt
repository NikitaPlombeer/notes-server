package ru.plombeer

import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.gson.gson
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.response.respondText
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.koin.core.KoinComponent
import ru.plombeer.controllers.Controller
import ru.plombeer.modules.injectControllers

class SimpleServer: KoinComponent {

    private val controllers: List<Controller> by injectControllers()

    fun startServer() {
        embeddedServer(Netty, port = 8080) {
            install(StatusPages) {
                exception<Throwable> { e ->
                    e.printStackTrace()
                    call.respondText(
                        text = e.localizedMessage,
                        contentType = ContentType.Text.Plain,
                        status = HttpStatusCode.InternalServerError
                    )
                }
            }
            install(ContentNegotiation) { gson() }
            controllers.forEach { it.applyApplication(this) }
        }.start(wait = true)
    }
}