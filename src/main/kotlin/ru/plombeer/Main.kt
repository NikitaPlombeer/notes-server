package ru.plombeer


import me.liuwj.ktorm.database.Database
import org.koin.core.context.startKoin
import ru.plombeer.modules.controllerModule
import ru.plombeer.modules.repositoryModule
import ru.plombeer.modules.useCaseModule

fun main() {
    println("Starting koin")
    startKoin {
        modules(listOf(controllerModule, repositoryModule, useCaseModule))
    }
    println("Connecting to DB")

    Database.connect(
        url = "jdbc:postgresql://localhost:7432/notes",
        user = "postgres",
        password = "1q2w3e4r",
        driver = "org.postgresql.Driver"
    )

    val server = SimpleServer()
    server.startServer()
}

data class StatusResponse(val status: String)
data class ResultResponse(val result: String)