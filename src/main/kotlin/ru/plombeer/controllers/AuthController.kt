package ru.plombeer.controllers

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.auth.jwt.jwt
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.routing
import ru.plombeer.usecase.JwtService
import ru.plombeer.ResultResponse
import ru.plombeer.usecase.users.FindUserByIdUseCase
import ru.plombeer.usecase.users.LoginUseCase
import ru.plombeer.usecase.users.RegisterUseCase
import ru.plombeer.views.request.RegisterRequest
import ru.plombeer.views.request.UserPasswordCredential
import ru.plombeer.views.response.UserView

class AuthController(
    private val findUserByIdUseCase: FindUserByIdUseCase,
    private val registerUseCase: RegisterUseCase,
    private val loginUseCase: LoginUseCase,
    private val jwtService: JwtService
) : Controller {

    override val order: Int = Int.MAX_VALUE

    override fun Application.applyController() {
        install(Authentication) {
            jwt {
                realm = jwtService.issuer
                verifier(jwtService.verifier)
                validate {
                    val userId = it.payload.getClaim("id").asLong()
                    findUserByIdUseCase.invoke(id = userId)
                }
            }
        }
        routing {
            post("/login") {
                val credentials = call.receive<UserPasswordCredential>()
                val loggedUserView = loginUseCase.invoke(credentials)
                call.respond(
                    message = ResultResponse(
                        result = loggedUserView.toToken()
                    )
                )
            }
            post("/register") {
                val request = call.receive<RegisterRequest>()
                val registeredUserView = registerUseCase.invoke(request)
                call.respond(
                    message = ResultResponse(
                        result = registeredUserView.toToken()
                    )
                )
            }
        }
    }

    private fun UserView.toToken() = jwtService.makeToken(this)

}

