package ru.plombeer.controllers

import io.ktor.application.Application

interface Controller {

    fun Application.applyController()

    fun applyApplication(app: Application) {
        with(app) {
            applyController()
        }
    }

    val order: Int
        get() = 0
}