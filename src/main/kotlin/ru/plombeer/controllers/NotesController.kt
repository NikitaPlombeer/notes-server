package ru.plombeer.controllers

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.auth.authentication
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.routing
import ru.plombeer.usecase.notes.GetNotesUseCase
import ru.plombeer.views.response.UserView

class NotesController(
    private val getNotesUseCase: GetNotesUseCase
) : Controller {

    override fun Application.applyController() {
        routing {
            authenticate {
                get("/notes") {
                    val user = call.authentication.principal<UserView>()!!
                    val notes = getNotesUseCase.invoke(user.id)
                    call.respond(notes)
                }
            }
        }
    }
}